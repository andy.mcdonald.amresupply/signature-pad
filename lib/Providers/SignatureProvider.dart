import 'dart:io';

import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';

class SignatureProvider extends GetConnect {
  @override
  void onInit() {
    httpClient.baseUrl = dotenv.env['server']!;
    httpClient.defaultContentType = 'application/json';
    super.onInit();
  }

  Future<bool> signatureUpload(List<String> invoices, String firstName,
      String lastName, File signature) async {
    Map<String, dynamic> data = <String, dynamic>{};
    data['invoices'] = invoices;
    data['first_name'] = firstName;
    data['last_name'] = lastName;
    data['signature'] = MultipartFile(signature, filename: 'temp.png');
    FormData form = FormData(data);
    final result = await post('pickup/signature_upload', form);
    if (result.statusCode == 200)
      return true;
    else
      return false;
  }
}
