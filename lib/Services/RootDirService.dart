// ignore_for_file: file_names

import 'package:get/get.dart';
import 'package:path_provider/path_provider.dart';

class RootDirService extends GetxService {
  late String dir;

  Future<RootDirService> init() async {
    dir = (await getApplicationDocumentsDirectory()).path;
    super.onInit();
    return this;
  }
}
