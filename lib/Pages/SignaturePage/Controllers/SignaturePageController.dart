// ignore_for_file: curly_braces_in_flow_control_structures

import 'dart:io';
import 'dart:ui';

import 'package:flutter/material.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:get/get.dart';
import 'package:signature_app/Pages/Homepage/Controllers/HomePageController.dart';
import 'package:signature_app/Pages/ThankYouPage/Views/ThankYouPage.dart';

import '../../../Services/RootDirService.dart';

class SignaturePageController extends GetxController {
  final signatureKey = GlobalKey<SignatureState>();
  File? signatureFile;
  Rx<bool> signed = false.obs;

  final RootDirService root = Get.find();
  final HomePageController hp = Get.find();

  void onSign() {
    signed.value = true;
  }

  void onReset() {
    signatureKey.currentState?.clear();
    signed.value = false;
  }

  void Function()? onOk() {
    if (signed.value == false)
      return null;
    else
      return () async {
        final bytes = await (await signatureKey.currentState!.getData())
            .toByteData(format: ImageByteFormat.png);
        signatureFile =
            File(root.dir + '/temp/' + hp.invoice.value.text + '.png');
        await signatureFile!.create(recursive: true);
        await signatureFile!.writeAsBytes(bytes!.buffer.asInt8List(),
            mode: FileMode.writeOnly, flush: true);
        Get.to(() => ThankYouPage());
      };
  }
}
