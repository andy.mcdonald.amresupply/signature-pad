import 'package:flutter/material.dart';
import 'package:flutter_signature_pad/flutter_signature_pad.dart';
import 'package:get/get.dart';
import 'package:signature_app/Pages/SignaturePage/Controllers/SignaturePageController.dart';

class SignaturePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var c = Get.put(SignaturePageController());
    return SafeArea(
      child: Container(
          decoration: BoxDecoration(
              gradient: LinearGradient(
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  colors: [Colors.grey, Colors.grey.shade100])),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              SizedBox.shrink(),
              Column(
                mainAxisSize: MainAxisSize.min,
                children: [
                  DecoratedSignature(
                    signatureKey: c.signatureKey,
                    onSign: c.onSign,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceAround,
                      children: [
                        ElevatedButton(
                            onPressed: c.onReset,
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Reset'),
                            )),
                        Obx(() => ElevatedButton(
                            onPressed: c.onOk(),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Text('Ok'),
                            )))
                      ],
                    ),
                  )
                ],
              )
            ],
          )),
    );
  }
}

class DecoratedSignature extends StatelessWidget {
  DecoratedSignature(
      {Key? key, required this.signatureKey, this.lastSignature, this.onSign})
      : super(key: key);

  final GlobalKey<SignatureState> signatureKey;
  final VoidCallback? onSign;
  Image? lastSignature;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Center(
          child: Container(
        height: 260,
        decoration: BoxDecoration(
          border: Border.all(color: Theme.of(context).primaryColor, width: 4.0),
          borderRadius: BorderRadius.circular(4),
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(4),
          child: lastSignature ??
              Signature(
                key: signatureKey,
                backgroundPainter: const WhiteBackground(),
                strokeWidth: 2,
                onSign: onSign,
              ),
        ),
      )),
    );
  }
}

class WhiteBackground extends CustomPainter {
  const WhiteBackground({Key? key});

  @override
  void paint(Canvas canvas, Size size) {
    final paint = Paint()
      ..style = PaintingStyle.fill
      ..color = Colors.white;

    canvas.drawPaint(paint);
  }

  @override
  bool shouldRepaint(WhiteBackground oldDelegate) => false;
}
