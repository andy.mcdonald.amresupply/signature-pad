import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:signature_app/Pages/ThankYouPage/Controllers/ThankYouPageController.dart';

class ThankYouPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    var c = Get.put(ThankYouPageController());
    return SafeArea(
        child: Material(
            child: GestureDetector(
      onDoubleTap: c.onDoubleTap,
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.grey, Colors.grey.shade100])),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                  child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  children: const [
                    Text(
                      'Thank you for your blah, blah, blah, yakity shmackity',
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      'Please pass this device back to the associate.',
                      textAlign: TextAlign.center,
                    ),
                  ],
                ),
              )),
            ),
          ],
        ),
      ),
    )));
  }
}
