import 'package:get/get.dart';
import 'package:signature_app/Pages/ConfirmationPage/Views/ConfirmationPage.dart';

class ThankYouPageController extends GetxController {
  void onDoubleTap() {
    Get.to(() => ConfirmationPage());
  }
}
