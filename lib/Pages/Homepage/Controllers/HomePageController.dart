import 'package:flutter/cupertino.dart';
import 'package:get/get.dart';
import 'package:signature_app/Pages/SignaturePage/Views/SignaturePage.dart';

class HomePageController extends GetxController {
  Rx<TextEditingController> invoice = TextEditingController().obs;
  Rx<TextEditingController> firstName = TextEditingController().obs;
  Rx<TextEditingController> lastName = TextEditingController().obs;
  RxList<String> invoices = <String>[].obs;
  FocusNode invoiceNode = FocusNode();
  FocusNode firstNameNode = FocusNode();
  FocusNode lastNameNode = FocusNode();

  @override
  void onInit() {
    invoice.value.addListener(() => invoice.refresh());
    firstName.value.addListener(() => firstName.refresh());
    lastName.value.addListener(() => lastName.refresh());
    super.onInit();
  }

  void submitInvoice(text) {
    invoices.add(text);
    invoice.value.text = '';
  }

  void onTapInvoice(index) {
    invoice.value.text = invoices[index];
    invoice.value.selection =
        TextSelection(baseOffset: 0, extentOffset: invoice.value.text.length);
    invoiceNode.requestFocus();
    invoices.removeAt(index);
  }

  void Function()? onNext() {
    if (invoice.value.text == '' ||
        firstName.value.text == '' ||
        lastName.value.text == '')
      return null;
    else
      return () => Get.to(SignaturePage());
  }

  void onReset() {}
}
