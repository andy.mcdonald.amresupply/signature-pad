// ignore_for_file: prefer_const_constructors

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:signature_app/Pages/Homepage/Controllers/HomePageController.dart';
import 'package:signature_app/Widgets/KeyboardWrapper.dart';

class Homepage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    SystemChrome.setEnabledSystemUIMode(SystemUiMode.immersiveSticky);
    var c = Get.put(HomePageController());
    return KeyboardWrapper(
        child: SafeArea(
            child: Material(
      child: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topCenter,
                end: Alignment.bottomCenter,
                colors: [Colors.grey, Colors.grey.shade100])),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Card(
                child: Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    // mainAxisAlignment: MainAxisAlignment.start,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      Row(
                        mainAxisSize: MainAxisSize.max,
                        children: [
                          Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('First Name',
                                        style: TextStyle(fontSize: 40)),
                                    TextField(
                                      controller: c.firstName.value,
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      style: TextStyle(fontSize: 30),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder()),
                                    ),
                                  ],
                                ),
                              )),
                          Expanded(
                              flex: 1,
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text('Last Name',
                                        style: TextStyle(fontSize: 40)),
                                    TextField(
                                      controller: c.lastName.value,
                                      textCapitalization:
                                          TextCapitalization.sentences,
                                      style: TextStyle(fontSize: 30),
                                      decoration: InputDecoration(
                                          border: OutlineInputBorder()),
                                    ),
                                  ],
                                ),
                              )),
                        ],
                      ),
                      Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Text('Invoices', style: TextStyle(fontSize: 40)),
                            TextField(
                              controller: c.invoice.value,
                              focusNode: c.invoiceNode,
                              style: TextStyle(fontSize: 30),
                              decoration: InputDecoration(
                                border: OutlineInputBorder(),
                              ),
                              keyboardType: TextInputType.number,
                              onSubmitted: c.submitInvoice,
                            ),
                            Container(
                              constraints: BoxConstraints(minHeight: 50),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Obx(() => Wrap(
                                      children: () {
                                        List<Widget> result = [];
                                        for (int i = 0;
                                            i < c.invoices.length;
                                            i++) {
                                          result.add(GestureDetector(
                                            onTap: () => c.onTapInvoice(i),
                                            child: Text(
                                                c.invoices[i] +
                                                    (i < c.invoices.length - 1
                                                        ? ', '
                                                        : ''),
                                                style: TextStyle(fontSize: 30)),
                                          ));
                                        }
                                        return result;
                                      }(),
                                    )),
                              ),
                            )
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: [
                  ElevatedButton(
                    onPressed: c.onReset,
                    child: Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Text('Reset'),
                    ),
                    style: ElevatedButton.styleFrom(
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(30)),
                        textStyle: TextStyle(fontSize: 40)),
                  ),
                  Obx(() => ElevatedButton(
                        onPressed: c.onNext(),
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: Text('Next'),
                        ),
                        style: ElevatedButton.styleFrom(
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(30)),
                            textStyle: TextStyle(fontSize: 40)),
                      )),
                ],
              ),
            )
          ],
        ),
      ),
    )));
  }
}
