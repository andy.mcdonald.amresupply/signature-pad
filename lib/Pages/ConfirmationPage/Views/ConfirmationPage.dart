import 'package:flutter/material.dart';
import 'package:flutter_layout_grid/flutter_layout_grid.dart';
import 'package:get/get.dart';
import 'package:signature_app/Pages/Homepage/Controllers/HomePageController.dart';
import 'package:signature_app/Pages/SignaturePage/Controllers/SignaturePageController.dart';

class ConfirmationPage extends StatelessWidget {
  final HomePageController hp = Get.find();
  final SignaturePageController sp = Get.find();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
        child: Material(
            child: Container(
                decoration: BoxDecoration(
                    gradient: LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.grey, Colors.grey.shade100])),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Card(
                        child: Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: LayoutGrid(
                            columnSizes: [auto, auto],
                            rowSizes: [auto, auto, auto, auto],
                            children: [
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Invoice:'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(hp.invoice.value.text),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('First Name:'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(hp.firstName.value.text),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Last Name:'),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text(hp.lastName.value.text),
                              ),
                              Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Signature:'),
                              ),
                              if (sp.signatureFile != null)
                                Padding(
                                  padding: const EdgeInsets.all(8.0),
                                  child: Image.file(sp.signatureFile!),
                                )
                            ],
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.all(8.0),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: [
                          ElevatedButton(
                              onPressed: () {},
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Cancel'),
                              )),
                          ElevatedButton(
                              onPressed: () {},
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Text('Submit'),
                              )),
                        ],
                      ),
                    ),
                  ],
                ))));
  }
}
