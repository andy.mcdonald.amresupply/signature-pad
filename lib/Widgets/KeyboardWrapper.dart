import 'package:flutter/material.dart';

class KeyboardWrapper extends StatelessWidget {
  const KeyboardWrapper({required this.child}) : super();
  final Widget child;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SingleChildScrollView(
            physics: const ClampingScrollPhysics(),
            child: ConstrainedBox(
                constraints: BoxConstraints(
                  minWidth: MediaQuery.of(context).size.width,
                  minHeight: MediaQuery.of(context).size.height,
                ),
                child: IntrinsicHeight(child: child))));
  }
}
