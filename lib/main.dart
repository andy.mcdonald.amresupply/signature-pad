import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get/get.dart';
// import 'package:flutter_dotenv/flutter_dotenv.dart';
import 'package:get_storage/get_storage.dart';

import 'Pages/Homepage/Views/Homepage.dart';
import 'Services/RootDirService.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await dotenv.load();
  // await GetStorage.init();
  await loadServices();
  runApp(GetMaterialApp(
    home: Homepage(),
    theme: theme,
  ));
}

Future<void> loadServices() async {
  // Get.put(AuthenticationService());
  await Get.putAsync(RootDirService().init);
}

ThemeData theme = ThemeData(
    textButtonTheme:
        TextButtonThemeData(style: TextButton.styleFrom(primary: Colors.black)),
    textTheme: const TextTheme(
      bodyText1: TextStyle(fontSize: 50, fontWeight: FontWeight.normal),
      bodyText2: TextStyle(fontSize: 50, fontWeight: FontWeight.normal),
    ),
    elevatedButtonTheme: ElevatedButtonThemeData(
      style: ElevatedButton.styleFrom(
          shape:
              RoundedRectangleBorder(borderRadius: BorderRadius.circular(30)),
          textStyle: TextStyle(fontSize: 40)),
    ));
